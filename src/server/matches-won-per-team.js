const papa = require('papaparse')
const fs = require('fs')
const path = require('path')

const convertCsvToJson = () => {

    const filePath = '../data/matches.csv'

    const fileContent = fs.readFileSync(filePath, 'utf8')
    const jsonData = papa.parse(fileContent, { header: true })

    return jsonData
}

const matchesWonPerTeam = (obj) => {

    let matchesWonPerTeamPerYear = {}

    let year = obj.data[0].season
    let flag = 0;

    obj.data.forEach(element => {
        if (element.season === year)
            flag++;
        else {
            matchesPerYear[year] = flag;
            year = element.season;
            flag = 1;
        }
    });

    return matchesPerYear;

}

const isWrittenOrNot = (result) => {

    try {

        const fileName = 'matchesPerYear_result.json';

        const resultFilePath = path.join(__dirname, '..', 'public', fileName)

        fs.writeFileSync(resultFilePath, JSON.stringify(result, null, 2), 'utf8')
    }
    catch {
        return "Error while writing the result into the file"
    }

    return "Output File successfully written at src/public"
}

const jsonData = convertCsvToJson();
const result = matchesPlayedPerYear(jsonData)
console.log(isWrittenOrNot(result))
